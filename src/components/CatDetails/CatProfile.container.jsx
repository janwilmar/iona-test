import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import actions from '../../modules/actions';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner'

class CatProfile extends React.Component {
    componentDidMount() {
        this.props.fetchCatDetails(this.props.match.params.id)
    }

    render() {
        let { cat_details, cat_details_error } = this.props;

        if (cat_details_error) {
            return <h3>Something went wrong! Please refresh page.</h3>;
        }
        if (!cat_details) {
            return <Spinner animation="grow" />;
        }
        return (
            <Card>
                <Card.Header>
                    <Button variant="primary" as={Link} to={{ pathname: '/', search: `?breed=${cat_details.breeds[0].id}` }}>Back</Button>
                </Card.Header>
                <Card.Img variant="top" src={cat_details.url} />
                <Card.Body>
                    <Card.Title>
                        <h4>{cat_details.breeds[0].name}</h4>
                        <h5>{`Origin: ${cat_details.breeds[0].origin}`}</h5>
                        <h6>{cat_details.breeds[0].temperament}</h6>
                    </Card.Title>
                    <Card.Text>
                        {cat_details.breeds[0].description}
                    </Card.Text>
                </Card.Body>
            </Card>
        );
    }
};

const mapDispatchToProps = {
    fetchCatDetails: actions.fetchCatDetails
};

const mapStateToProps = (state) => ({
    cat_details: state.cat_details,
    cat_details_loading: state.cat_details_loading,
    cat_details_error: state.cat_details_error,
});

CatProfile = connect(mapStateToProps, mapDispatchToProps)(CatProfile)

export default withRouter(CatProfile);
