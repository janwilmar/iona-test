import React from 'react';
import './CatDetails.css';
import CatProfile from './CatProfile.container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';

function CatDetails() {
    return (
        <Container className="cat-profile-cont">
            <Row>
                <Col sm={12} md={12} lg={12}><CatProfile /></Col>
            </Row>
        </Container>
    );
}

export default CatDetails;