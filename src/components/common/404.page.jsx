import React from 'react';
import { useLocation } from 'react-router-dom';

function NotFound() {
    return (
        <div>
            URL - {useLocation().pathname} not found!
        </div>
    );
}

export default NotFound;