import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import CardColumns from 'react-bootstrap/CardColumns';

class BreedSearchList extends React.Component {
    render() {
        let { breed_search, breed_search_error } = this.props;

        if (breed_search_error) {
            return <h3>Something went wrong! Please try again.</h3>;
        }
        return (
            <CardColumns>
                {breed_search ?
                    breed_search.map((v, i) => (
                        <Card key={i}>
                            <Card.Img variant="top" src={v.url} />
                            <Card.Body>
                                <Button variant="primary" as={Link} to={`/${v.id}`}>View Details</Button>
                            </Card.Body>
                        </Card>
                    ))
                    : null
                }
            </CardColumns >
        );
    }
};

const mapStateToProps = (state) => ({
    breed_search: state.breed_search,
    breed_search_loading: state.breed_search_loading,
    breed_search_error: state.breed_search_error
});

BreedSearchList = connect(mapStateToProps, null)(BreedSearchList)

export default BreedSearchList;
