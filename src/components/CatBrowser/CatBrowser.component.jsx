import React from 'react';
import BreedsList from './BreedsList.container';
import BreedSearchList from './BreedSearchList.container';
import BreedLoadMore from './BreedLoadMore.container';
import './CatBrowser.css';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';


function CatBrowser() {
    return (
        <Container className="cat-browser-cont">
            <Row>
                <Col sm={6} md={6} lg={6}><h1>Cat Browser</h1></Col>
            </Row>
            <Row>
                <Col sm={4} md={3} lg={3}><BreedsList /></Col>
            </Row>
            <Row>
                <Col sm={12} md={12} lg={12}><BreedSearchList /></Col>
            </Row>
            <Row>
                <Col sm={6} md={6} lg={6}><BreedLoadMore /></Col>
            </Row>
        </Container>
    );
}

export default CatBrowser;