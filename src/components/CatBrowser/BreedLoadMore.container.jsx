import React from 'react';
import { connect } from 'react-redux';
import actions from '../../modules/actions';
import Button from 'react-bootstrap/Button';


class BreedLoadMore extends React.Component {
    loadMore() {
        let { breed_id } = this.props;
        this.props.fetchBreedSearch(breed_id, true);
    }

    render() {
        let { breed_search_error, breed_id, load_more, breed_search_loading } = this.props;
        if (breed_search_error) {
            return <h3>Something went wrong! Please try again.</h3>;
        }
        if (!load_more) return null;
        return (
            <Button
                variant="success"
                onClick={this.loadMore.bind(this)}
                disabled={(breed_search_loading) ? true : breed_id ? false : true}>
                {breed_search_loading ? 'Loading Cats...' : 'Load More'}
            </Button>
        );
    }
};

const mapDispatchToProps = {
    fetchBreedSearch: actions.fetchBreedSearch
};

const mapStateToProps = (state) => ({
    load_more: state.load_more,
    breed_id: state.breed_id,
    breed_search_loading: state.breed_search_loading,
    breed_search_error: state.breed_search_error,
});

BreedLoadMore = connect(mapStateToProps, mapDispatchToProps)(BreedLoadMore)

export default BreedLoadMore;
