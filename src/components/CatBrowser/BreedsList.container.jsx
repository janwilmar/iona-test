import React from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import actions from '../../modules/actions';
import { parse } from 'querystring';
import Form from 'react-bootstrap/Form';


class BreedsList extends React.Component {
    state = {
        breed_value: ""
    }

    componentDidMount() {
        let qs_param = parse(this.props.location.search);
        this.props.fetchBreeds();

        if (qs_param['?breed']) {
            this.setState({ breed_value: qs_param['?breed'] });
            this.props.fetchBreedSearch(qs_param['?breed'], false);
        }
    }

    selectBreed(event) {
        const select_value = event.target.value;
        this.setState({ breed_value: select_value });
        if (select_value.trim() === "") {
            this.props.clearBreedSearch();
        } else {
            this.props.fetchBreedSearch(select_value, false);
        }
    }

    render() {
        let { breeds, breeds_error, breeds_loading, breed_search_loading } = this.props;
        if (breeds_error) {
            return <select disabled><option>Something went wrong! Please refresh page.</option></select>;
        }
        return (
            <Form.Group controlId="breed-list-select-form">
                <Form.Label>Breeds</Form.Label>
                <Form.Control
                    as="select"
                    disabled={breeds_loading || breed_search_loading}
                    onChange={this.selectBreed.bind(this)}
                    value={this.state.breed_value}>
                    <option value="">Select breed</option>
                    {breeds ?
                        breeds.map((v, i) => <option key={i} value={v.id}>{v.name}</option>)
                        : null
                    }
                </Form.Control>
            </Form.Group>
        );
    }
};

const mapDispatchToProps = {
    fetchBreeds: actions.fetchBreeds,
    fetchBreedSearch: actions.fetchBreedSearch,
    clearBreedSearch: actions.clearBreedSearch,
};

const mapStateToProps = (state) => ({
    breeds: state.breeds,
    breeds_loading: state.breeds_loading,
    breeds_error: state.breeds_error,
    breed_search_loading: state.breed_search_loading,
});

BreedsList = connect(mapStateToProps, mapDispatchToProps)(BreedsList)

export default withRouter(BreedsList);
