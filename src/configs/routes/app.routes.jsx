import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { CatBrowser } from '../../components/CatBrowser';
import { CatDetails } from '../../components/CatDetails';
import { NotFound } from '../../components/common';


export default <div>
    <Switch>
        <Route exact path="/"><CatBrowser /></Route>
        <Route exact path="/:id"><CatDetails /></Route>
        <Route path="*"><NotFound /></Route>
    </Switch>
</div>