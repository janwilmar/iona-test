const _array = require('lodash/array');

const initialState = {
    breeds: null,
    breeds_loading: false,
    breeds_error: false,
    detail_id: null,

    breed_search: null,
    breed_search_loading: false,
    breed_search_error: false,
    breed_id: null,
    load_more: true,

    cat_details_loading: false,
    cat_details: null, 
    cat_details_error: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'REQUEST_BREEDS':
            return {
                ...state,
                breeds_loading: true,
                breeds_error: false,
            };
        case 'REQUEST_BREEDS_SUCCESS':
            return {
                ...state,
                breeds: action.breeds,
                breeds_loading: false,
                breeds_error: false,
            };
        case 'REQUEST_BREEDS_FAIL':
            return {
                ...state,
                breeds_loading: false,
                breeds_error: true,
            };
        case 'REQUEST_BREED_SEARCH':
            return {
                ...state,
                load_more: true,
                breed_search_page: 1,
                breed_search_limit: 10,
                breed_id: null,
                breed_search: null,
                breed_search_loading: true,
                breed_search_error: false,
            };
        case 'REQUEST_BREED_SEARCH_SUCCESS':
            let breed_search = action.breed_search
            return {
                ...state,
                breed_id: action.breed_id,
                breed_search: breed_search,
                breed_search_loading: false,
                breed_search_error: false,
            };
        case 'REQUEST_BREED_SEARCH_FAIL':
            return {
                ...state,
                breed_search_loading: false,
                breed_search_error: true,
            };
        case 'LOAD_MORE_BREED_SEARCH':
            return {
                ...state,
                breed_search_page: state.breed_search_page + 1,
                breed_search_limit: state.breed_search_limit,
                breed_search_loading: true,
                breed_search_error: false,
            };
        case 'LOAD_MORE_BREED_SEARCH_SUCCESS':
            let new_breed_search = _array.uniqBy([...state.breed_search, ...action.breed_search], 'id');
            let diff = _array.differenceBy(new_breed_search, state.breed_search, 'id');
            return {
                ...state,
                breed_search: new_breed_search,
                breed_search_loading: false,
                breed_search_error: false,
                load_more: (diff.length > 0) ? true : false,
            };
        case 'LOAD_MORE_BREED_SEARCH_FAIL':
            return {
                ...state,
                breed_search_loading: false,
                breed_search_error: true,
            };
        case 'CLEAR_BREED_SEARCH':
            return {
                ...state,
                breed_id: null,
                breed_search: null,
                breed_search_loading: false,
                breed_search_error: false,
            };
        case 'REQUEST_CAT_DETAILS':
            return {
                ...state,
                cat_details_loading: true,
                cat_details_error: false,
            };
        case 'REQUEST_CAT_DETAILS_SUCCESS':
            return {
                ...state,
                cat_details: action.cat_details,
                cat_details_loading: false,
                cat_details_error: false,
            };
        case 'REQUEST_CAT_DETAILS_FAIL':
            return {
                ...state,
                cat_details_loading: false,
                cat_details_error: true,
            };
        default:
            return state;
    }
}

export default reducer;