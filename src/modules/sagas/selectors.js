const breed_search_page = (state) => state.breed_search_page;
const breed_search_limit = (state) => state.breed_search_limit;

export {
    breed_search_page,
    breed_search_limit
}