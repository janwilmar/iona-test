import { put, call, takeEvery, all, select } from 'redux-saga/effects';
import axios from 'axios';

import actions from '../actions';
import * as selector from './selectors';

function* fetchBreedsAsync() {
    try {
        yield put(actions.requestBreeds());
        const data = yield call(() => {
            return axios.get('https://api.thecatapi.com/v1/breeds')
                .then(res => res.data)
        });
        yield put(actions.requestBreedsSuccess(data));
    } catch (error) {
        yield put(actions.requestBreedsError());
    }
}

function* fetchBreedsWatcher() {
    yield takeEvery('FETCH_BREEDS', fetchBreedsAsync);
}

function* fetchBreedSearchAsync(action) {
    try {
        if (action.load_more) {
            yield put(actions.loadMoreBreedSearch());
        } else {
            yield put(actions.requestBreedSearch());
        }

        const page = yield select(selector.breed_search_page);
        const limit = yield select(selector.breed_search_limit);

        const data = yield call(() => {
            return axios.get('https://api.thecatapi.com/v1/images/search', { params: { page, limit, breed_id: action.breed_id } })
                .then(res => res.data)
        });

        if (action.load_more) {
            yield put(actions.loadMoreBreedSearchSuccess(data));
        } else {
            yield put(actions.requestBreedSearchSuccess(action.breed_id, data));
        }

    } catch (error) {
        if (action.load_more) {
            yield put(actions.loadMoreBreedSearchError());
        } else {
            yield put(actions.requestBreedSearchError());
        }
    }
}

function* fetchBreedSearchWatcher() {
    yield takeEvery('FETCH_BREED_SEARCH', fetchBreedSearchAsync);
}

function* fetchCatDetailsAsync(action) {
    try {
        yield put(actions.requestCatDetails());
        const data = yield call(() => {
            return axios.get(`https://api.thecatapi.com/v1/images/${action.cat_id}`)
                .then(res => res.data)
        });
        yield put(actions.requestCatDetailsSuccess(data));
    } catch (error) {
        yield put(actions.requestCatDetailsError());
    }
}

function* fetchCatDetailsWatcher() {
    yield takeEvery('FETCH_CAT_DETAILS', fetchCatDetailsAsync);
}

export default function* rootSaga() {
    yield all([
        fetchBreedsWatcher(),
        fetchBreedSearchWatcher(),
        fetchCatDetailsWatcher(),
    ])
}