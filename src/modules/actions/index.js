const requestBreeds = () => {
    return { type: 'REQUEST_BREEDS' };
}

const requestBreedsSuccess = (data) => {
    return { type: 'REQUEST_BREEDS_SUCCESS', breeds: data };
};

const requestBreedsError = () => {
    return { type: 'REQUEST_BREEDS_FAIL' };
};

const fetchBreeds = () => {
    return { type: 'FETCH_BREEDS' };
}

const requestBreedSearch = () => {
    return { type: 'REQUEST_BREED_SEARCH' };
}

const requestBreedSearchSuccess = (breed_id, data) => {
    return { type: 'REQUEST_BREED_SEARCH_SUCCESS', breed_search: data, breed_id };
};

const requestBreedSearchError = () => {
    return { type: 'REQUEST_BREED_SEARCH_FAIL' };
};

const fetchBreedSearch = (breed_id, load_more) => {
    return { type: 'FETCH_BREED_SEARCH', breed_id, load_more };
}

const loadMoreBreedSearch = () => {
    return { type: 'LOAD_MORE_BREED_SEARCH' };
}

const loadMoreBreedSearchSuccess = (data) => {
    return { type: 'LOAD_MORE_BREED_SEARCH_SUCCESS', breed_search: data };
};

const loadMoreBreedSearchError = () => {
    return { type: 'LOAD_MORE_BREED_SEARCH_FAIL' };
};

const clearBreedSearch = () => {
    return { type: 'CLEAR_BREED_SEARCH' };
}

const requestCatDetails = () => {
    return { type: 'REQUEST_CAT_DETAILS' };
}

const requestCatDetailsSuccess = (data) => {
    return { type: 'REQUEST_CAT_DETAILS_SUCCESS', cat_details: data };
};

const requestCatDetailsError = () => {
    return { type: 'REQUEST_CAT_DETAILS_FAIL' };
};

const fetchCatDetails = (cat_id) => {
    return { type: 'FETCH_CAT_DETAILS', cat_id };
} 

export default {
    requestBreeds,
    requestBreedsSuccess,
    requestBreedsError,
    fetchBreeds,

    requestBreedSearch,
    requestBreedSearchSuccess,
    requestBreedSearchError,
    fetchBreedSearch,

    loadMoreBreedSearch,
    loadMoreBreedSearchSuccess,
    loadMoreBreedSearchError,

    clearBreedSearch,

    requestCatDetails,
    requestCatDetailsSuccess,
    requestCatDetailsError,
    fetchCatDetails
};